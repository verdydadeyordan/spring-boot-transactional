package com.study.architecture.booking.controller;

import com.study.architecture.booking.entity.dao.Employee;
import com.study.architecture.booking.entity.dto.CreateEmployeeDto;
import com.study.architecture.booking.service.EmployeeAggregatorService;
import com.study.architecture.booking.utililty.BusinessException;
import io.swagger.annotations.Api;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "User Employee Controller", description = "REST API for Employee")
@RequestMapping("/employee/trx")
@RestController
public class EmployeeTransactionController {

  @Autowired
  private EmployeeAggregatorService employeeAggregatorService;

  @GetMapping("/{id}")
  public Employee getEmployee(HttpServletRequest request,
      @PathVariable("id") long id) {
    return employeeAggregatorService.findById(id);
  }

  @PostMapping("/create/no-trx")
  public Employee postNoTrx(HttpServletRequest request,
      @RequestBody CreateEmployeeDto createEmployeeDto) {
    return employeeAggregatorService.postNoTrx(createEmployeeDto);
  }

  @PostMapping("/create/no-trx/manual")
  public Employee postNoTrxManual(HttpServletRequest request,
      @RequestBody CreateEmployeeDto createEmployeeDto) {
    return employeeAggregatorService.postNoTrxManual(createEmployeeDto);
  }

  @PostMapping("/create/no-trx/failed")
  public Employee postNoTrxFailed(HttpServletRequest request,
      @RequestBody CreateEmployeeDto createEmployeeDto) {
    return employeeAggregatorService.postNoTrxFailed(createEmployeeDto);
  }

  @PostMapping("/create/trx-mandatory")
  public Employee postTrxMandatory(HttpServletRequest request,
      @RequestBody CreateEmployeeDto createEmployeeDto) {
    return employeeAggregatorService.postTrxMandatory(createEmployeeDto);
  }

  @PostMapping("/create/trx-mandatory/error")
  public Employee postTrxMandatoryError(HttpServletRequest request,
      @RequestBody CreateEmployeeDto createEmployeeDto) {
    return employeeAggregatorService.postTrxMandatoryError(createEmployeeDto);
  }

  @PostMapping("/create/trx-supports")
  public Employee postTrxSupport(HttpServletRequest request,
      @RequestBody CreateEmployeeDto createEmployeeDto) {
    return employeeAggregatorService.postTrxSupport(createEmployeeDto);
  }

  @PostMapping("/create/trx-supports/no-trx")
  public Employee postTrxSupportNoTrx(HttpServletRequest request,
      @RequestBody CreateEmployeeDto createEmployeeDto) {
    return employeeAggregatorService.postTrxSupportNoTrx(createEmployeeDto);
  }

  @PostMapping("/create/trx-required")
  public Employee postTrxRequired(HttpServletRequest request,
      @RequestBody CreateEmployeeDto createEmployeeDto) throws BusinessException {
    return employeeAggregatorService.postTrxRequired(createEmployeeDto);
  }

  @PostMapping("/create/trx-required/error")
  public Employee postTrxRequiredError(HttpServletRequest request,
      @RequestBody CreateEmployeeDto createEmployeeDto) throws BusinessException {
    return employeeAggregatorService.postTrxRequiredError(createEmployeeDto);
  }

  @PostMapping("/create/trx-required/rollback-handler")
  public Employee postTrxRequiredRollbackHandler(HttpServletRequest request,
      @RequestBody CreateEmployeeDto createEmployeeDto) throws BusinessException {
    return employeeAggregatorService.postTrxRequiredRollbackHandler(createEmployeeDto);
  }

  @PostMapping("/create/trx-requires-new")
  public Employee postTrxRequiresNew(HttpServletRequest request,
      @RequestBody CreateEmployeeDto createEmployeeDto) throws BusinessException {
    return employeeAggregatorService.postTrxRequiresNew(createEmployeeDto);
  }

  @PostMapping("/create/trx-never")
  public Employee postTrxNever(HttpServletRequest request,
      @RequestBody CreateEmployeeDto createEmployeeDto) throws BusinessException {
    return employeeAggregatorService.postTrxNever(createEmployeeDto);
  }

  @PostMapping("/create/trx-not-supported")
  public Employee postTrxNotSupported(HttpServletRequest request,
      @RequestBody CreateEmployeeDto createEmployeeDto) throws BusinessException {
    return employeeAggregatorService.postTrxNotSupported(createEmployeeDto);
  }

  @PostMapping("/create/trx-manual")
  public Employee postTrxManual(HttpServletRequest request,
      @RequestBody CreateEmployeeDto createEmployeeDto) throws BusinessException {
    return employeeAggregatorService.postTrxManual(createEmployeeDto);
  }

  @PostMapping("/create/trx-manual/error")
  public Employee postTrxManualError(HttpServletRequest request,
      @RequestBody CreateEmployeeDto createEmployeeDto) throws BusinessException {
    return employeeAggregatorService.postTrxManualError(createEmployeeDto);
  }

  @PostMapping("/create/trx-exception/expected")
  public Employee postTrxExpectationExpected(HttpServletRequest request,
      @RequestBody CreateEmployeeDto createEmployeeDto) throws BusinessException {
    return employeeAggregatorService.postTrxExpectationExpected(createEmployeeDto);
  }

  @PostMapping("/create/trx-exception/unexpected")
  public Employee postTrxExpectationUnexpected(HttpServletRequest request,
      @RequestBody CreateEmployeeDto createEmployeeDto) throws BusinessException {
    return employeeAggregatorService.postTrxExpectationUnexpected(createEmployeeDto);
  }

  @PostMapping("/create/trx-timeout")
  public Employee postTrxTimeout(HttpServletRequest request,
      @RequestBody CreateEmployeeDto createEmployeeDto) throws BusinessException {
    return employeeAggregatorService.postTrxTimeout(createEmployeeDto);
  }

  @PutMapping("/update/trx-isolation/{id}")
  public Employee putTrxRequired(HttpServletRequest request,
      @PathVariable("id") long id,
      @RequestBody CreateEmployeeDto createEmployeeDto) throws BusinessException {
    return employeeAggregatorService.putTrxRequired(id, createEmployeeDto);
  }

}
