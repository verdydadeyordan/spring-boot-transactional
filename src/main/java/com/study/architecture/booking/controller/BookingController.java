package com.study.architecture.booking.controller;

import io.swagger.annotations.Api;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "User Booking Controller", description = "REST API for Booking")
@RequestMapping("/booking")
@RestController
public class BookingController {

  @Value(("${server.port}"))
  private String port;

  @GetMapping
  public String getBooking(HttpServletRequest request) {
    return "hello from " + port + ", request id = " + request.getHeader("X-Request-Id");
  }

  @PostMapping
  public String postBooking(HttpServletRequest request) {
    return "hello from " + port + ", request id = " + request.getHeader("X-Request-Id");
  }

  @PutMapping
  public String putBooking(HttpServletRequest request) {
    return "hello from " + port + ", request id = " + request.getHeader("X-Request-Id");
  }

  @DeleteMapping
  public String deleteBooking(HttpServletRequest request) {
    return "hello from " + port + ", request id = " + request.getHeader("X-Request-Id");
  }

  @PatchMapping
  public String patchBooking(HttpServletRequest request) {
    return "hello from " + port + ", request id = " + request.getHeader("X-Request-Id");
  }
}
