package com.study.architecture.booking.repository;


import com.study.architecture.booking.entity.dao.Contact;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactRepository extends CrudRepository<Contact, Long> {

  Contact findFirstByEmployeeId(long employeeId);
}
