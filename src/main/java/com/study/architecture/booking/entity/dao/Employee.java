package com.study.architecture.booking.entity.dao;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "employee")
public class Employee {

  @Id
  @Column(name = "id", unique = true)
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @Column(name = "emp_name")
  private String empName;

  @Column(name = "birth_date")
  private Date birthDate;

  @Transient
  private Contact contact;
}
