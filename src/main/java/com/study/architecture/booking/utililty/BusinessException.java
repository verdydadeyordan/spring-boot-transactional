package com.study.architecture.booking.utililty;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BusinessException extends RuntimeException {

  private String code;
  private String message;

  public BusinessException(String code, String message) {
    super();
    this.setCode(code);
    this.setMessage(message);
  }
}
