package com.study.architecture.booking.utililty;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ExpectedBusinessException extends Throwable {

  private String code;
  private String message;

  public ExpectedBusinessException(String code, String message) {
    super();
    this.setCode(code);
    this.setMessage(message);
  }
}
