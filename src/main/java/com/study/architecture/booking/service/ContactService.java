package com.study.architecture.booking.service;

import com.study.architecture.booking.entity.dao.Contact;
import com.study.architecture.booking.utililty.BusinessException;

public interface ContactService {

  Contact findById(long id);

  Contact findByEmployeeId(long employeeId);

  void delete(long id);

  void deleteNoTrx(long id);

  Contact postNoTrx(Contact contact);

  Contact postTrxMandatory(Contact contact);

  Contact postTrxSupport(Contact contact);

  Contact postTrxRequired(Contact contact);

  Contact postTrxRequiresNew(Contact contact);

  Contact postTrxNever(Contact contact);

  Contact postTrxNotSupported(Contact contact);

  Contact postError(Contact contact) throws BusinessException;

}
