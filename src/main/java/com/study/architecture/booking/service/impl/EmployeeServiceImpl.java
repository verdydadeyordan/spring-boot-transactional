package com.study.architecture.booking.service.impl;

import com.study.architecture.booking.entity.dao.Employee;
import com.study.architecture.booking.repository.EmployeeRepository;
import com.study.architecture.booking.service.EmployeeService;
import com.study.architecture.booking.utililty.BusinessException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EmployeeServiceImpl implements EmployeeService {

  @Autowired
  private EmployeeRepository employeeRepository;

  @Autowired
  private SessionFactory sessionFactory;

  @Override
  @Transactional(readOnly = true)
  public Employee findById(long id) {
    return employeeRepository.findById(id)
        .orElse(null);
  }

  @Override
  public Employee postNoTrx(Employee employee) {
    return employeeRepository.save(employee);
  }

  @Override
  @Transactional(propagation = Propagation.MANDATORY, isolation = Isolation.READ_COMMITTED)
  public Employee postTrxMandatory(Employee employee) {
    return employeeRepository.save(employee);
  }

  @Override
  @Transactional(propagation = Propagation.SUPPORTS)
  public Employee postTrxSupport(Employee employee) {
    return employeeRepository.save(employee);
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public Employee postTrxRequired(Employee employee) {
    return employeeRepository.save(employee);
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public Employee postTrxRequiresNew(Employee employee) {
    return employeeRepository.save(employee);
  }

  @Override
  @Transactional(propagation = Propagation.NEVER)
  public Employee postTrxNever(Employee employee) {
    return employeeRepository.save(employee);
  }

  @Override
  @Transactional(propagation = Propagation.NOT_SUPPORTED)
  public Employee postTrxNotSupported(Employee employee) {
    return employeeRepository.save(employee);
  }

  @Override
  public Employee postTrxManual(Employee employee) {
    Session session = sessionFactory.openSession();
    // lets open up a transaction. remember setAutocommit(false)!
    session.beginTransaction();

    session.save(employee);
    // and commit it
    session.getTransaction().commit();

    // close the session == our jdbc connection
    session.close();
    return employee;
  }

  @Override
  public Employee postTrxManualError(Employee employee) throws BusinessException {
    Session session = sessionFactory.openSession();
    try {
      // lets open up a transaction. remember setAutocommit(false)!
      session.beginTransaction();

      session.save(employee);
      throw new BusinessException("test", "test");
    } catch (Exception e) {
      // and rollback it
      session.getTransaction().rollback();
    } finally {
      session.close();
    }
    throw new BusinessException("test", "test");
  }

  @Override
  public Employee postError(Employee employee) throws BusinessException {
    throw new BusinessException("test", "test");
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public Employee putTrxRequiredAndIsolation(long id, Employee employee) {
    Employee existing = findById(id);
    existing.setEmpName(employee.getEmpName());
    existing.setBirthDate(employee.getBirthDate());
    return employeeRepository.save(existing);
  }

  @Override
  @Transactional(propagation = Propagation.MANDATORY)
  public void delete(long id) {
    employeeRepository.deleteById(id);
  }

  @Override
  public void deleteNoTrx(long id) {
    employeeRepository.deleteById(id);
  }

  @Override
  public void deleteNoTrxError(long id) {
    throw new BusinessException("test", "test");
  }

}
