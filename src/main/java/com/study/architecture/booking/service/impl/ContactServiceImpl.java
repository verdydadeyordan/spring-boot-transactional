package com.study.architecture.booking.service.impl;

import com.study.architecture.booking.entity.dao.Contact;
import com.study.architecture.booking.repository.ContactRepository;
import com.study.architecture.booking.service.ContactService;
import com.study.architecture.booking.utililty.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ContactServiceImpl implements ContactService {

  @Autowired
  private ContactRepository contactRepository;

  @Override
  @Transactional(readOnly = true)
  public Contact findById(long id) {
    return contactRepository.findById(id)
        .orElse(null);
  }

  @Override
  @Transactional(readOnly = true)
  public Contact findByEmployeeId(long employeeId) {
    return contactRepository.findFirstByEmployeeId(employeeId);
  }

  @Override
  public Contact postNoTrx(Contact contact) {
    return contactRepository.save(contact);
  }

  @Override
  @Transactional(propagation = Propagation.MANDATORY, isolation = Isolation.READ_COMMITTED)
  public Contact postTrxMandatory(Contact contact) {
    return contactRepository.save(contact);
  }

  @Override
  @Transactional(propagation = Propagation.SUPPORTS)
  public Contact postTrxSupport(Contact contact) {
    return contactRepository.save(contact);
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public Contact postTrxRequired(Contact contact) {
    return contactRepository.save(contact);
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public Contact postTrxRequiresNew(Contact contact) {
    return contactRepository.save(contact);
  }

  @Override
  @Transactional(propagation = Propagation.NEVER)
  public Contact postTrxNever(Contact contact) {
    return contactRepository.save(contact);
  }

  @Override
  @Transactional(propagation = Propagation.NOT_SUPPORTED)
  public Contact postTrxNotSupported(Contact contact) {
    return contactRepository.save(contact);
  }

  @Override
  public Contact postError(Contact contact) throws BusinessException {
    throw new BusinessException("test", "test");
  }

  @Override
  @Transactional(propagation = Propagation.MANDATORY)
  public void delete(long id) {
    contactRepository.deleteById(id);
  }

  @Override
  public void deleteNoTrx(long id) {
    contactRepository.deleteById(id);
  }

}
