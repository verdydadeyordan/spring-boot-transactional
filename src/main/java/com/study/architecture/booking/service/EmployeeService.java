package com.study.architecture.booking.service;

import com.study.architecture.booking.entity.dao.Employee;
import com.study.architecture.booking.utililty.BusinessException;

public interface EmployeeService {

  Employee findById(long id);

  void delete(long id);

  void deleteNoTrx(long id);

  void deleteNoTrxError(long id);

  Employee postNoTrx(Employee employee);

  Employee postTrxMandatory(Employee employee);

  Employee postTrxSupport(Employee employee);

  Employee postTrxRequired(Employee employee);

  Employee postTrxRequiresNew(Employee employee);

  Employee postTrxNever(Employee employee);

  Employee postTrxNotSupported(Employee employee);

  Employee postTrxManual(Employee employee);

  Employee postTrxManualError(Employee employee);

  Employee postError(Employee employee) throws BusinessException;

  Employee putTrxRequiredAndIsolation(long id, Employee employee);

}
