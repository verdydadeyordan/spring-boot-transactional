package com.study.architecture.booking.service;

import com.study.architecture.booking.entity.dao.Employee;
import com.study.architecture.booking.entity.dto.CreateEmployeeDto;
import com.study.architecture.booking.utililty.BusinessException;

public interface EmployeeAggregatorService {

  Employee findById(long id);

  Employee postNoTrx(CreateEmployeeDto createEmployeeDto);

  Employee postNoTrxManual(CreateEmployeeDto createEmployeeDto);

  Employee postNoTrxFailed(CreateEmployeeDto createEmployeeDto);

  Employee postTrxMandatory(CreateEmployeeDto createEmployeeDto);

  Employee postTrxMandatoryError(CreateEmployeeDto createEmployeeDto);

  Employee postTrxSupport(CreateEmployeeDto createEmployeeDto);

  Employee postTrxSupportNoTrx(CreateEmployeeDto createEmployeeDto);

  Employee postTrxRequired(CreateEmployeeDto createEmployeeDto);

  Employee postTrxRequiredError(CreateEmployeeDto createEmployeeDto) throws BusinessException;

  Employee postTrxRequiredRollbackHandler(CreateEmployeeDto createEmployeeDto);

  Employee postTrxRequiresNew(CreateEmployeeDto createEmployeeDto);

  Employee postTrxNever(CreateEmployeeDto createEmployeeDto);

  Employee postTrxNotSupported(CreateEmployeeDto createEmployeeDto);

  Employee postTrxManual(CreateEmployeeDto createEmployeeDto);

  Employee postTrxManualError(CreateEmployeeDto createEmployeeDto);

  Employee postTrxExpectationExpected(CreateEmployeeDto createEmployeeDto) throws BusinessException;

  Employee postTrxExpectationUnexpected(CreateEmployeeDto createEmployeeDto);

  Employee postTrxTimeout(CreateEmployeeDto createEmployeeDto);

  Employee putTrxRequired(long id, CreateEmployeeDto createEmployeeDto);
}
