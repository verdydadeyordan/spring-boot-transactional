package com.study.architecture.booking.service.impl;

import com.study.architecture.booking.entity.dao.Contact;
import com.study.architecture.booking.entity.dao.Employee;
import com.study.architecture.booking.entity.dto.CreateEmployeeDto;
import com.study.architecture.booking.service.ContactService;
import com.study.architecture.booking.service.EmployeeAggregatorService;
import com.study.architecture.booking.service.EmployeeService;
import com.study.architecture.booking.utililty.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

//REQUIRED to tell Spring to either join an active transaction or to start a new one if the method gets called without a transaction. This is the default behavior.
//SUPPORTS to join an activate transaction if one exists. If the method gets called without an active transaction, this method will be executed without a transactional context.
//MANDATORY to join an activate transaction if one exists or to throw an Exception if the method gets called without an active transaction.
//NEVER to throw an Exception if the method gets called in the context of an active transaction.
//NOT_SUPPORTED to suspend an active transaction and to execute the method without any transactional context.
//REQUIRES_NEW to always start a new transaction for this method. If the method gets called with an active transaction, that transaction gets suspended until this method got executed.
//NESTED to start a new transaction if the method gets called without an active transaction. If it gets called with an active transaction, Spring sets a savepoint and rolls back to that savepoint if an Exception occurs.

@Slf4j
@Service
public class EmployeeAggregatorServiceImpl implements EmployeeAggregatorService {

  @Autowired
  private EmployeeService employeeService;

  @Autowired
  private ContactService contactService;

  @Override
  public Employee findById(long id) {
    Employee employee = employeeService.findById(id);
    employee.setContact(contactService.findByEmployeeId(id));
    return employee;
  }

  @Override
  public Employee postNoTrx(CreateEmployeeDto createEmployeeDto) {
    Employee employee = employeeService.postNoTrx(constructEmployee(createEmployeeDto));
    Contact contact = contactService.postNoTrx(constructContact(createEmployeeDto, employee));
    employee.setContact(contact);
    return employee;
  }

  @Override
  public Employee postNoTrxManual(CreateEmployeeDto createEmployeeDto) {
    try {
      Employee employee = employeeService.postNoTrx(constructEmployee(createEmployeeDto));

      try {
        Contact contact = contactService.postError(constructContact(createEmployeeDto, employee));
        employee.setContact(contact);
        return employee;
      } catch (Exception e) {
        log.error("{}", e);
        employeeService.deleteNoTrx(employee.getId());
        throw new BusinessException("test", "test");
      }

    } catch (Exception e) {
      log.error("{}", e);
      throw new BusinessException("test", "test");
    }
  }

  @Override
  public Employee postNoTrxFailed(CreateEmployeeDto createEmployeeDto) {
    try {
      Employee employee = employeeService.postNoTrx(constructEmployee(createEmployeeDto));

      try {
        Contact contact = contactService.postError(constructContact(createEmployeeDto, employee));
        employee.setContact(contact);
        return employee;
      } catch (Exception e) {
        log.error("{}", e);
        employeeService.deleteNoTrxError(employee.getId());
        throw new BusinessException("test", "test");
      }

    } catch (Exception e) {
      log.error("{}", e);
      throw new BusinessException("test", "test");
    }
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public Employee postTrxMandatory(CreateEmployeeDto createEmployeeDto) {
    Employee employee = employeeService.postTrxMandatory(constructEmployee(createEmployeeDto));
    Contact contact = contactService
        .postTrxMandatory(constructContact(createEmployeeDto, employee));
    employee.setContact(contact);
    return employee;
  }

  @Override
  public Employee postTrxMandatoryError(CreateEmployeeDto createEmployeeDto) {
    Employee employee = employeeService.postTrxRequired(constructEmployee(createEmployeeDto));
    Contact contact = contactService
        .postTrxMandatory(constructContact(createEmployeeDto, employee));
    employee.setContact(contact);
    return employee;
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public Employee postTrxSupport(CreateEmployeeDto createEmployeeDto) {
    Employee employee = employeeService.postTrxSupport(constructEmployee(createEmployeeDto));
    Contact contact = contactService.postError(constructContact(createEmployeeDto, employee));
    employee.setContact(contact);
    return employee;
  }

  @Override
  public Employee postTrxSupportNoTrx(CreateEmployeeDto createEmployeeDto) {
    Employee employee = employeeService.postTrxSupport(constructEmployee(createEmployeeDto));
    Contact contact = contactService.postError(constructContact(createEmployeeDto, employee));
    employee.setContact(contact);
    return employee;
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public Employee postTrxRequired(CreateEmployeeDto createEmployeeDto) {
    Employee employee = employeeService.postTrxRequired(constructEmployee(createEmployeeDto));
    Contact contact = contactService.postTrxRequired(constructContact(createEmployeeDto, employee));
    employee.setContact(contact);
    return employee;
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public Employee postTrxRequiredError(CreateEmployeeDto createEmployeeDto)
      throws BusinessException {
    Employee employee = employeeService.postTrxRequired(constructEmployee(createEmployeeDto));
    Contact contact = contactService.postTrxRequired(constructContact(createEmployeeDto, employee));
    employee.setContact(contact);
    throw new BusinessException("test", "test");
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public Employee postTrxRequiredRollbackHandler(CreateEmployeeDto createEmployeeDto) {
    Employee employee = new Employee();
    try {
      employee = employeeService.postError(constructEmployee(createEmployeeDto));
    } catch (Exception e) {
      log.error("{}", e);
    }
    Contact contact = contactService.postTrxRequired(constructContact(createEmployeeDto, employee));
    employee.setContact(contact);
    return employee;
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public Employee postTrxRequiresNew(CreateEmployeeDto createEmployeeDto) {
    Employee employee = employeeService.postTrxRequiresNew(constructEmployee(createEmployeeDto));
    Contact contact = contactService
        .postTrxRequired(constructContact(createEmployeeDto, employee));

    employee.setContact(contact);
    return employee;
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public Employee postTrxNever(CreateEmployeeDto createEmployeeDto) {
    Employee employee = employeeService.postNoTrx(constructEmployee(createEmployeeDto));
    Contact contact = contactService.postError(constructContact(createEmployeeDto, employee));
    employee.setContact(contact);
    return employee;
  }

  @Override
  public Employee postTrxNotSupported(CreateEmployeeDto createEmployeeDto) {
    return employeeService.postTrxManual(constructEmployee(createEmployeeDto));
  }

  @Override
  public Employee postTrxManual(CreateEmployeeDto createEmployeeDto) {
    return employeeService.postTrxManual(constructEmployee(createEmployeeDto));
  }

  @Override
  public Employee postTrxManualError(CreateEmployeeDto createEmployeeDto) {
    return employeeService.postTrxManualError(constructEmployee(createEmployeeDto));
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED,
      rollbackFor = NullPointerException.class)
  public Employee postTrxExpectationExpected(CreateEmployeeDto createEmployeeDto)
      throws BusinessException {
    Employee employee = employeeService.postTrxMandatory(constructEmployee(createEmployeeDto));
    Contact contact = contactService
        .postTrxMandatory(constructContact(createEmployeeDto, employee));
    employee.setContact(contact);
    throw new BusinessException("test", "test");
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED,
      noRollbackFor = BusinessException.class)
  public Employee postTrxExpectationUnexpected(CreateEmployeeDto createEmployeeDto) {
    Employee employee = employeeService.postTrxRequired(constructEmployee(createEmployeeDto));
    Contact contact = contactService.postTrxRequired(constructContact(createEmployeeDto, employee));
    employee.setContact(contact);
    throw new BusinessException("test", "test");
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED,
      timeout = 1
      // in second
  )
  public Employee postTrxTimeout(CreateEmployeeDto createEmployeeDto) {
    Employee employee = employeeService.postTrxRequired(constructEmployee(createEmployeeDto));
    Contact contact = contactService.postTrxRequired(constructContact(createEmployeeDto, employee));
    employee.setContact(contact);
    try {
      Thread.sleep(2000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    return employee;
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRED)
  public Employee putTrxRequired(long id, CreateEmployeeDto createEmployeeDto) {
    Employee employee = employeeService
        .putTrxRequiredAndIsolation(id, constructEmployee(createEmployeeDto));

    Employee employee2 = employeeService.findById(id);
    return employee;
  }

  private Employee constructEmployee(CreateEmployeeDto createEmployeeDto) {
    return Employee.builder()
        .empName(createEmployeeDto.getEmpName())
        .birthDate(createEmployeeDto.getBirthDate())
        .build();
  }

  private Contact constructContact(CreateEmployeeDto createEmployeeDto, Employee employee) {
    return Contact.builder()
        .employeeId(employee.getId())
        .phoneNumber(createEmployeeDto.getContact().getPhoneNumber())
        .email(createEmployeeDto.getContact().getEmail())
        .build();
  }
}
