# Spring Boot Transactional

## Note
If you don't know the Transactional concept, you can read this blog
[Spring Transaction Management](https://vladmihalcea.com/spring-transactional-annotation/).
I think this blog is good enough to understand the concept. 

## How To Run The Project
mvn clean install

## Database
MySQL

## Swagger URL
http://localhost:8082/swagger-ui/index.html


##REQUIRED 
to tell Spring to either join an active transaction or to start a new one if the method gets called without a transaction. This is the default behavior.
##SUPPORTS 
to join an activate transaction if one exists. If the method gets called without an active transaction, this method will be executed without a transactional context.
##MANDATORY 
to join an activate transaction if one exists or to throw an Exception if the method gets called without an active transaction.
##NEVER 
to throw an Exception if the method gets called in the context of an active transaction.
##NOT_SUPPORTED 
to suspend an active transaction and to execute the method without any transactional context.
##REQUIRES_NEW 
to always start a new transaction for this method. If the method gets called with an active transaction, that transaction gets suspended until this method got executed.
##NESTED 
to start a new transaction if the method gets called without an active transaction. If it gets called with an active transaction, Spring sets a savepoint and rolls back to that savepoint if an Exception occurs.
